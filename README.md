# SPAs en Routing
Veel moderne web-apps (en mobiele apps) zijn tegenwoordig SPAs (**S**ingle **P**age **A**pplication). Dit is een website die bestaat uit maar één pagina waar alles in draait. In React heet dit de virtuele DOM, de DOM ken je wel van HTML, React neemt dit een stapje verder.

Met de router kun je je app dan opsplitsen in meerdere pagina's. De router observeert dan de URL en voert op basis van wijzigingen in de URL acties uit, zoals het wijzigen van een pagina.

Dit princiepe kennen jullie al een beetje van MVC, maar in React werkt het toch anders. Waarom? Omdat het met JavaScript gaat. Alles wat er gebeurd blijft binnen dezelfde HTML pagina, als je van pagina naar pagina navigeert herlaad de pagina niet. Alles gebeurd in de virtuele DOM.

Dat was de theorie voor vandaag, op naar de morgen! Of nee, toch niet.

### Aan de slag
We gaan onze app van vorige week opsplitsen in meerdere paginas. Voor nu houden we het op de homepagina en een contact pagina. Om jullie creativiteit te bevorderen laat ik het design van de contact pagina aan jullie over. De stappen die benodigd zijn tot het resultaat lopen we wel samen door.

We beginnen met een nieuwe installatie van een Reatc app, waarom? Herhaling! In deze app gaan we dan ook de router implementeren, daarna is het aan jullie om de app van vorige week te implementeren in deze nieuwe app. 

Maak een nieuwe React app in de keuzedeel-fd folder (C:/xampp/htdocs/keuzedeel-fd). 
```bash
npm create vite@latest react-router
```
Navigeer naar het project met `cd` en installeer de volgende NPM packages om de router te kunnen gebruiken.
```bash
npm install react-router-dom localforage match-sorter sort-by bootstrap react-bootstrap
```
Als NPM klaar is met de packages installeren, dan kunnen we aan de slag. Start je VSCode in deze map (`code .`) en open `src/main.js`.

Begin eerst met het importeren van Bootstrap zoals we dat in de vorige weken hebben gedaan, dat betekent dus:
- index.css wordt index.scss
- importeer bootstrap in index.scss
- update main.jsx met het nieuwe scss bestand

Vanaf hier gaan we over op de [documentatie van React Router](https://reactrouter.com/en/main/start/tutorial#adding-a-router). Maar omdat dit voor sommige lastig kan zijn gaan we er samen door heen.

Om gebruik te kunnen maken van de router, moeten wij eerst de router importeren, daarna kunnen we een instantie van de router aanmaken. 
```jsx
import { StrictMode } from 'react'
import { createRoot } from 'react-dom/client'
import './index.scss'
import App from './App.jsx'

createRoot(document.getElementById('root')).render(
  <StrictMode>
    <App />
  </StrictMode>,
)

```

Goed, nu hebben we in onze index.js aangegeven dat we gebruik willen maken van de router en hebben we een instantie van de router aangemaakt. De RouteProvider zorgt voor alle magie, die handelt alles netjes af. Momenteel gebeurd er nog niks, omdat wij nog moeten aangeven welke pagina bij welke route hoort. Dit word alleen enorm lastig als je geen pagina's hebt.

Je raad het al, de volgende stap word dus het aanmaken van pagina's, dit doen wij in `src/routes`. Deze map bestaat nog niet, dus maak deze aan. In deze map maken wij een bestand genaamd `Home.js` en een bestand genaamd `Contact.js`. Deze bestanden zullen gaan functioneren als onze twee paginas.

```jsx
// Home.js
import { Link } from "react-router-dom";

export default function Home() {
    return (
    <>
        Je zit nu op home, <Link to='/contact'>naar contact</Link>.
    </>
    )
}
```

```jsx
// Contact.js
import { Link } from "react-router-dom";

export default function Contact() {
    return (
    <>
        Je zit nu op contact, <Link to='/contact'>naar home</Link>.
    </>
    )
}
```

De laatste stap is nu om tegen de router te zeggen welke pagina bij welke URL hoort. Dit is redelijk simpel en kan in `index.js`.
```jsx
import Home from './routes/Home';
import Contact from './routes/Contact';

const router = createBrowserRouter([
  {
    path: "/",
    element: <Home />,
  },
  {
    path: "/contact",
    element: <Contact />,
  },
]);
```
Zoals je kunt zien gebeurd hier niks bijzonders, we importeren onze paginas en we zeggen tegen de router dat de pagina `Home` bij de url `/` hoort, en `Contact` bij de url `/contact`. In onze componenten maken we gebruik van de `<Link />` component, deze is van React Router en zorgt er voor dat de URL veranderingen binnen de virtuele DOM gebeuren.

## De opdracht
Gefeliciteerd, je hebt een werkende router! Nu is het tijd om de opdracht van vorige week te implementeren in deze router app. Houd rekening met de volgende puntent:
- Zorg dat de items in de navbar navigeren naar de juiste pagina, gebruik de `<Link />` component.
- Maak zelf een contact pagina, deze bestaat uit:
  - Een veld voor de naam.
  - Een veld voor een email adres.
  - Een textarea voor het bericht.
  - Een button om te submitten.
  - Input velden zijn componenten!
  - Alles is in bootstrap of een ander framework!
